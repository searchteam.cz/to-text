# -*- coding: UTF-8 -*-
"""
Auth.graphql.

Graphql | Controller | Entering-point for auth module

:copyright: andering@gmail.com
:license: BSD-3-Clause
"""

import graphene
import tempfile
import textract
from base64 import b64decode


class Type(graphene.Enum):
    """
    Type.

    Type to use with graphene
    """

    CSV = 1
    DOC = 2
    DOCX = 3
    EML = 4
    EPUB = 5
    GIF = 6
    JPG = 7
    JSON = 8
    HTML = 9
    MP3 = 10
    MSG = 11
    ODT = 12
    OGG = 13
    PDF = 14
    PNG = 15
    PPTX = 16
    PS = 17
    RTF = 18
    TIFF = 19
    TXT = 20
    WAV = 21
    XLSX = 22
    XLS = 23


class Query(graphene.ObjectType):
    """
    Query.

    Query to use with graphene
    """

    bytes = graphene.Field(
        type=graphene.String,
        bytes=graphene.Argument(graphene.String),
        extension=graphene.Argument(Type)
    )

    def resolve_bytes(parent, info, bytes, extension):
        """
        Bytes handler.

        Handling bytes entering point
        """
        temp = tempfile.NamedTemporaryFile(suffix='.pdf')
        try:
            temp.write(b64decode(bytes))
            text = textract.process(temp.name).decode('utf-8')
        finally:
            temp.close()

        return text
