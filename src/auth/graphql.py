# -*- coding: UTF-8 -*-
"""
Auth.graphql.

Graphql | Controller | Entering-point for auth module

:copyright: andering@gmail.com
:license: BSD-3-Clause
"""

import graphene
import flask
from graphql import GraphQLError
from flask_jwt_extended import create_access_token


class Query(graphene.ObjectType):
    """
    Query.

    Query to use with graphene
    """

    auth = graphene.Field(
        type=graphene.String,
        apikey=graphene.String(required=True),
    )

    def resolve_auth(parent, info, apikey):
        """
        Auth handler.

        Handling auth entering point
        """
        if apikey != flask.current_app.config['API_KEY']:
            raise GraphQLError("Wrong ApiKey")

        access_token = create_access_token(identity=apikey)

        return access_token
