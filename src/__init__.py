# -*- coding: UTF-8 -*-
"""
totext.__init__.

Entering point for totext

:copyright: andering@gmail.com
:license: BSD-3-Clause
"""

import graphene
import os
import flask
import click

from . import totext, auth
from .config import config
from flask_graphql import GraphQLView
from flask_jwt_extended import JWTManager, jwt_optional, get_jwt_identity


def create_app():
    """
    totext App Factory.

    Main App
    """
    app = flask.Flask(__name__)
    app.config.from_object(config[os.environ["FLASK_ENV"]])
    JWTManager(app)

    class Query(totext.graphql.Query, auth.graphql.Query):
        """
        Query.

        Query to use with graphene
        """

        pass

    @app.route('/', methods=['GET'])
    def init1():
        return 'aaaa'

    @app.route('/healthz', methods=['GET'])
    def init2():
        return 'aaaa'

    @app.route('/graphql', methods=['POST'])
    @jwt_optional
    def graphql():
        """
        Entering point Graphene.

        Graphene
        """
        return GraphQLView.as_view(
            'graphql',
            schema=graphene.Schema(query=Query),
            get_context=lambda: {'logged_in_as': get_jwt_identity()}
        )()

    @app.cli.command('test')
    def test():
        return 'test'

    return app
