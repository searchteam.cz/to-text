import io

from pip.req import parse_requirements
from setuptools import find_packages
from setuptools import setup

with io.open("README.rst", "rt", encoding="utf8") as f:
    readme = f.read()

setup(
    name="to-text",
    version="0.5.0",
    license="BSD",
    maintainer="andering",
    maintainer_email="andering@gmail.com",
    description="to-text",
    long_description=readme,
    packages=parse_requirements('requirements.pip', session='hack'),
    include_package_data=True,
    zip_safe=False,
    install_requires=["src"],
    extras_require={"test": ["pytest", "coverage"]},
)
