FROM python:3.7-slim
RUN apt-get update && apt-get install -y python-dev libxml2-dev libxslt1-dev antiword unrtf poppler-utils  tesseract-ocr flac ffmpeg lame libmad0 libsox-fmt-mp3 sox libjpeg-dev swig  python python-dev python-pip build-essential swig git libpulse-dev libasound2-dev

COPY src /app/src
COPY uwsgi.ini /app
COPY wsgi.py /app
COPY requirements.pip /app

WORKDIR /app
RUN bash -c "python3 -m venv .venv && source .venv/bin/activate && pip install --upgrade pip && pip install -r requirements.pip"

ENTRYPOINT ["bash"]
CMD ["-c","source .venv/bin/activate && uwsgi --ini uwsgi.ini"]
